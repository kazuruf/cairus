
// remove a meta tag da versão wp e do feed rss
add_filter('the_generator', '__return_false');


// remove o parâmetro de versão do wp de qualquer script enqueado
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'at_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'at_remove_wp_ver_css_js', 9999 );
