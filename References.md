Referências
===========



Um grande conjunto de regras não são as regras criadas pelos proprietários do repositório, no entanto, elas são inspiradas por alguma outra entrada online.
Esta página tentará capturar todas essas fontes de referências quando detectadas.


Lista sem ordem específica

http://www.slideshare.net/erchetansoni/complete-wordpress-security-by-chetan-soni-sr-security-specialist-at-secugenius-security-solutions
http://seclists.org/fulldisclosure/2013/Jul/38
https://github.com/anantshri/wp-security
https://github.com/ethicalhack3r/wordpress_plugin_security_testing_cheat_sheet
https://github.com/m4ll0k/WPSeku
http://journalxtra.com/websiteadvice/wordpress-security-hardening-htaccess-rules-4025/
